from elastic import Elastic
import json
from elasticsearch import Elasticsearch
from config import ELASTIC_HOSTS, ELASTIC_SETTINGS
import nltk
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import json
# nltk.download('punkt')
# nltk.download('stopwords')
index_name = "collectiontsv"
elastic = Elastic(index_name)
es = Elasticsearch(hosts=ELASTIC_HOSTS)
tex = ""
stop_words = set(stopwords.words('english'))
with open('C:/Users/alborzsystem/Desktop/data/training/train_topics_v1.0.json') as json_file:
    data = json.load(json_file)
    turn = 0

    for p in data:
        turn += 1

        for s in  p['turn']:
            print('query: ' + s['raw_utterance'])

            if s['number'] == 1:
                filtered_string = ''
            rank = int(0)

            word_tokens = word_tokenize(s['raw_utterance'])

            filtered_array = [word for word in word_tokens if not word in stop_words]

            filtered_array = []
            #stop:removing stop words from query using nltk
            for w in word_tokens:
                if w not in stop_words:
                    filtered_array.append(w)
            #rep:replacing questioning and not informative words
            #.replace("?", "").replace("What", "").replace("How", "").replace("When", "").replace(".", "").replace("Describe", '').replace
            #concat:concating all queries in one turn
            filtered_string = filtered_string + ' ' + ' '.join(filtered_array).replace("?", "").replace("What", "").replace("How", "").replace("When", "").replace(".", "").replace("Describe", '').replace("Tell", '')

            result = elastic.search(
                filtered_string, "content", 100)
            for i in elastic.search(
                    filtered_string, "content", 100):
                rank = int(rank) + int(1)
                if not i.startswith("CAR"):
                    docid = "MARCO_" + i
                else:
                    docid=i
                #writing results in trec format
                tex=tex+(str(turn) + "_" + str(s['number']) + " 0 " + docid + " " + str(rank) + " " + str(result[i]["score"]) + " hi\n")

f = open("demofile2concatrepstop.txt", "a")
f.write(tex)
f.close()



