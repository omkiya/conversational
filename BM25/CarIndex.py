
"""
Car Indexer
===========

Car indexing example for testing purposes.

:Authors: Krisztian Balog, Faegheh Hasibi
small edits by Omid Mohammadi Kia
"""
from elastic import Elastic
import json
from elasticsearch import Elasticsearch
from config import ELASTIC_HOSTS, ELASTIC_SETTINGS
import csv

def main():
    index_name = "collectiontsv"

    mappings = {
        "content": Elastic.analyzed_field(),
    }


    elastic = Elastic(index_name)
    es = Elasticsearch(hosts=ELASTIC_HOSTS)

    body = 0

    dict = {}
    #first we should convert cbor file to xml
    #using trec car tools
    #https://github.com/gla-ial/trec-cast-tools
    with open("D:/cbor.xml") as infile:
        for line in infile:
            if (line.startswith("<DOCNO>")):
                left = "<DOCNO>"
                right = "</DOCNO>"
                docid = line[line.index(left) + len(left):line.index(right)]

            elif (line.startswith("<BODY>")):
                body = 1
            #before and after body is new line we extract only body
            elif (body >= 1):
                body = body + 1
                if (body == 2):
                    bod = line.rstrip()

                    body = 0

                    dic = {}
                    dic['content'] = bod
                    dict[docid] = dic

            else:
                body = 0

    if (es.indices.exists(index_name)):
        elastic.delete_index()
        elastic.add_docs_bulk(dict)
        print(" index updated")

    else:
        elastic.create_index(mappings)
        elastic.delete_index()
        elastic.add_docs_bulk(dict)
        print("new index created")

if __name__ == "__main__":
    main()





