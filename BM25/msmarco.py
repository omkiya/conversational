"""
msmarco Indexer
===========

msmarco indexing example for testing purposes.

:Authors: Krisztian Balog, Faegheh Hasibi
small edits by Omid Mohammadi Kia
"""
from elastic import Elastic
import json
from elasticsearch import Elasticsearch
from config import ELASTIC_HOSTS, ELASTIC_SETTINGS
import csv

def main():
    index_name = "collectiontsv"

    mappings = {
        "content": Elastic.analyzed_field(),
    }

    alldocs = {}
    elastic = Elastic(index_name)
    es = Elasticsearch(hosts=ELASTIC_HOSTS)
    with open("collection.tsv") as tsvfile:
        tsvreader = csv.reader(tsvfile, delimiter="\t")
        # i=0
        for line in tsvreader:
            # if(i==20):
            #   break
            # else:
            #print(line[0:])
            doc = {}
            doc['content'] = line[1]
            alldocs[line[0]] = doc
            #i = i + 1


    if (es.indices.exists(index_name)):
        elastic.add_docs_bulk(dict)
        print(" index updated")

    else:
        elastic.create_index(mappings)
        elastic.add_docs_bulk(dict)
        print("new index created")


if __name__ == "__main__":
    main()





